#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "ble_advdata.h"
#include "nordic_common.h"
#include "softdevice_handler.h"
#include "bsp_btn_ble.h"
#include "nrf_drv_saadc.h"
#include "nrf_drv_ppi.h"
#include "nrf_drv_timer.h"
#include "nrf_drv_config.h"
#include "nrf_drv_gpiote.h"
#include "nrf_delay.h"

#include "nrf_gpiote.h"
#include "boards.h"
#include "app_error.h"
#include "app_util_platform.h"
#include "app_timer.h"
#include "app_util_platform.h"

#include "boards.h"

/*         NAME RULE
function : get_value, set_value
custom enum, constant value : BATTERY_ADC, TEMP_ADC
value : chargeState, nameValue
internal value : charge_state;
struct : test_values_t, self_values_t
*/

#define GPIOTE_CHANNEL_0 0

#define BUTTON_PIN_NUMBER 6

#define NUS_SERVICE_UUID_TYPE           BLE_UUID_TYPE_VENDOR_BEGIN                  /**< UUID type for the Nordic UART Service (vendor specific). */

#define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(5000, APP_TIMER_PRESCALER)  /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(30000, APP_TIMER_PRESCALER) /**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT    3                                           /**< Number of attempts before giving up the connection parameter negotiation. */

#define APP_ADV_INTERVAL                64                                          /**< The advertising interval (in units of 0.625 ms. This value corresponds to 40 ms). */
#define APP_ADV_TIMEOUT_IN_SECONDS      180                                         /**< The advertising timeout (in units of seconds). */
#define MIN_CONN_INTERVAL               MSEC_TO_UNITS(20, UNIT_1_25_MS)             /**< Minimum acceptable connection interval (20 ms), Connection interval uses 1.25 ms units. */
#define MAX_CONN_INTERVAL               MSEC_TO_UNITS(75, UNIT_1_25_MS)             /**< Maximum acceptable connection interval (75 ms), Connection interval uses 1.25 ms units. */
#define SLAVE_LATENCY                   0                                           /**< Slave latency. */
#define CONN_SUP_TIMEOUT                MSEC_TO_UNITS(4000, UNIT_10_MS)             /**< Connection supervisory timeout (4 seconds), Supervision Timeout uses 10 ms units. */
#define DEVICE_NAME                     "Nordic_UART"                               /**< Name of device. Will be included in the advertising data. */

#define CENTRAL_LINK_COUNT              0                                 /**<number of central links used by the application. When changing this number remember to adjust the RAM settings*/
#define PERIPHERAL_LINK_COUNT           0                                 /**<number of peripheral links used by the application. When changing this number remember to adjust the RAM settings*/

#define IS_SRVC_CHANGED_CHARACT_PRESENT 0                                 /**< Include or not the service_changed characteristic. if not enabled, the server's database cannot be changed for the lifetime of the device*/

#define APP_CFG_NON_CONN_ADV_TIMEOUT    0                                 /**< Time for which the device must be advertising in non-connectable mode (in seconds). 0 disables timeout. */
#define NON_CONNECTABLE_ADV_INTERVAL    MSEC_TO_UNITS(100, UNIT_0_625_MS) /**< The advertising interval for non-connectable advertisement (100 ms). This value can vary between 100ms to 10.24s). */

#define APP_BEACON_INFO_LENGTH          0x18                              /**< Total length of information advertised by the Beacon. */
#define APP_ADV_DATA_LENGTH             0x15                              /**< Length of manufacturer specific data in the advertisement. */
#define APP_DEVICE_TYPE                 0x02                              /**< 0x02 refers to Beacon. */
#define APP_MEASURED_RSSI               0xC3                              /**< The Beacon's measured RSSI at 1 meter distance in dBm. */
#define APP_COMPANY_IDENTIFIER          0x0059                            /**< Company identifier for Nordic Semiconductor ASA. as per www.bluetooth.org. */
#define APP_MAJOR_VALUE                 0x01, 0x02                        /**< Major value used to identify Beacons. */
#define APP_MINOR_VALUE                 0x03, 0x04                        /**< Minor value used to identify Beacons. */
#define APP_BEACON_UUID                 0x01, 0x12, 0x23, 0x34, \
										0x45, 0x56, 0x67, 0x78, \
										0x89, 0x9a, 0xab, 0xbc, \
                                        0xcd, 0xde, 0xef, 0xf0            /**< Proprietary UUID for Beacon. */

#define DEAD_BEEF                       0xDEADBEEF                        /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */

#define APP_TIMER_PRESCALER             0                                 /**< Value of the RTC1 PRESCALER register. */
#define APP_TIMER_OP_QUEUE_SIZE         4                                 /**< Size of timer operation queues. */

#if defined(USE_UICR_FOR_MAJ_MIN_VALUES)
#define MAJ_VAL_OFFSET_IN_BEACON_INFO   18                                /**< Position of the MSB of the Major Value in m_beacon_info array. */
#define UICR_ADDRESS                    0x10001080                        /**< Address of the UICR register used by this example. The major and minor versions to be encoded into the advertising data will be picked up from this location. */
#endif

#ifndef NRF_APP_PRIORITY_HIGH
#define NRF_APP_PRIORITY_HIGH 1
#endif
#define UART_TX_BUF_SIZE 256 /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE 1   /**< UART RX buffer size. */

#define SAMPLES_IN_BUFFER 2

#define BTN_PRESSED 0
#define BTN_RELEASED 1

#define LED_1_PIN                12 //BSP_LED_0
#define BUTTON_1_PIN  		6 //BSP_BUTTON_0
#define USB_CHECK_PIN 7
#define CHARGE_OK_PIN 8


const uint8_t crc8_table[256] = {
	0,  94, 188, 226,  97,  63, 221, 131, 194, 156, 126,  32, 163, 253,  31,  65,
	157, 195,  33, 127, 252, 162,  64,  30,  95,   1, 227, 189,  62,  96, 130, 220,
	35, 125, 159, 193,  66,  28, 254, 160, 225, 191,  93,   3, 128, 222,  60,  98,
	190, 224,   2,  92, 223, 129,  99,  61, 124,  34, 192, 158,  29,  67, 161, 255,
	70,  24, 250, 164,  39, 121, 155, 197, 132, 218,  56, 102, 229, 187,  89,   7,
	219, 133, 103,  57, 186, 228,   6,  88,  25,  71, 165, 251, 120,  38, 196, 154,
	101,  59, 217, 135,   4,  90, 184, 230, 167, 249,  27,  69, 198, 152, 122,  36,
	248, 166,  68,  26, 153, 199,  37, 123,  58, 100, 134, 216,  91,   5, 231, 185,
	140, 210,  48, 110, 237, 179,  81,  15,  78,  16, 242, 172,  47, 113, 147, 205,
	17,  79, 173, 243, 112,  46, 204, 146, 211, 141, 111,  49, 178, 236,  14,  80,
	175, 241,  19,  77, 206, 144, 114,  44, 109,  51, 209, 143,  12,  82, 176, 238,
	50, 108, 142, 208,  83,  13, 239, 177, 240, 174,  76,  18, 145, 207,  45, 115,
	202, 148, 118,  40, 171, 245,  23,  73,   8,  86, 180, 234, 105,  55, 213, 139,
	87,   9, 235, 181,  54, 104, 138, 212, 149, 203,  41, 119, 244, 170,  72,  22,
	233, 183,  85,  11, 136, 214,  52, 106,  43, 117, 151, 201,  74,  20, 246, 168,
	116,  42, 200, 150,  21,  75, 169, 247, 182, 232,  10,  84, 215, 137, 107,  53
};

typedef enum {
	RUNNING = 0x1,
	SLEEP = 0x2
} POWER_STATE;

typedef enum {
	CHARGE_NONE = 0x0,
	CHARGE_NOW = 0x1,
	CHARGE_DONE = 0x2
} CHARGE_STATE;

typedef struct {
	uint8_t temp[2];
	uint8_t battery[2];
} packet_data_t;

//인터럽트 등에서 사용하는 변수
volatile CHARGE_STATE chargeState;
volatile POWER_STATE powerState;
volatile packet_data_t packetData;

//인터럽트 등에서 사용하지 않는 변수
nrf_ppi_channel_t m_ppi_channel1;
ble_gap_adv_params_t advParams;  
uint32_t timerCount;

void saadc_callback(nrf_drv_saadc_evt_t const * p_event);

void timer_handler1(nrf_timer_event_t event_type, void* p_context){
}

void saadc_sampling_event_init(void){
	
	const nrf_drv_timer_t   m_timer1={
		.p_reg            = CONCAT_2(NRF_TIMER, 1),              \
		.instance_id      = CONCAT_3(TIMER, 1, _INSTANCE_INDEX), \
		.cc_channel_count = NRF_TIMER_CC_CHANNEL_COUNT(1),       \
	};

	ret_code_t err_code;
	err_code = nrf_drv_ppi_init();
	APP_ERROR_CHECK(err_code);

	err_code = nrf_drv_timer_init(&m_timer1, NULL, timer_handler1);
	APP_ERROR_CHECK(err_code);

    /* setup m_timer for compare event every 400ms */
	uint32_t ticks = nrf_drv_timer_ms_to_ticks(&m_timer1, 100);
	nrf_drv_timer_extended_compare(&m_timer1, NRF_TIMER_CC_CHANNEL0, ticks, NRF_TIMER_SHORT_COMPARE0_CLEAR_MASK, false);
	nrf_drv_timer_enable(&m_timer1);

	uint32_t timer_compare_event_addr = nrf_drv_timer_compare_event_address_get(&m_timer1, NRF_TIMER_CC_CHANNEL0);
	uint32_t saadc_sample_event_addr = nrf_drv_saadc_task_address_get(NRF_SAADC_TASK_SAMPLE);

    /* setup ppi channel so that timer compare event is triggering sample task in SAADC */
	err_code = nrf_drv_ppi_channel_alloc(&m_ppi_channel1);

	APP_ERROR_CHECK(err_code);

	err_code = nrf_drv_ppi_channel_assign(m_ppi_channel1, timer_compare_event_addr, saadc_sample_event_addr);
	APP_ERROR_CHECK(err_code);

}

void saadc_samxpling_event_enable(void){
	ret_code_t err_code = nrf_drv_ppi_channel_enable(m_ppi_channel1);
	APP_ERROR_CHECK(err_code);
}


void saadc_init(void){
	ret_code_t err_code;
	
	nrf_saadc_value_t       m_buffer_pool1[2][SAMPLES_IN_BUFFER];
	nrf_saadc_channel_config_t channel_config =
	NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN0);
	channel_config.gain = NRF_SAADC_GAIN1_4;
	channel_config.reference = NRF_SAADC_REFERENCE_VDD4;
	nrf_saadc_channel_config_t channel_config2 =
	NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN1);
	channel_config2.gain = NRF_SAADC_GAIN1_4;
	channel_config2.reference = NRF_SAADC_REFERENCE_VDD4;

	err_code = nrf_drv_saadc_init(NULL, saadc_callback);
	APP_ERROR_CHECK(err_code);

	err_code = nrf_drv_saadc_channel_init(0, &channel_config);
	APP_ERROR_CHECK(err_code);
	err_code = nrf_drv_saadc_channel_init(1, &channel_config2);
	APP_ERROR_CHECK(err_code);

	err_code = nrf_drv_saadc_buffer_convert(m_buffer_pool1[0],SAMPLES_IN_BUFFER);
	APP_ERROR_CHECK(err_code);
	err_code = nrf_drv_saadc_buffer_convert(m_buffer_pool1[1],SAMPLES_IN_BUFFER);
	APP_ERROR_CHECK(err_code);
}

void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name){
	app_error_handler(DEAD_BEEF, line_num, p_file_name);
}

uint8_t crc_8(unsigned char *data, int size)  {  
	size_t lp;
	uint8_t crc8 = 0;
	for(lp = 0;lp < size;lp++)	 	
		crc8 = crc8_table[crc8 ^ data[lp]];
	return crc8;
}

static void advertising_init(){
	uint32_t      err_code;
	ble_advdata_t advdata;

	uint8_t       flags = BLE_GAP_ADV_FLAG_BR_EDR_NOT_SUPPORTED;
	uint8_t                  adv_manuf_data_data[24];
	ble_advdata_manuf_data_t manuf_specific_data;
	
	manuf_specific_data.company_identifier = APP_COMPANY_IDENTIFIER;

	adv_manuf_data_data[0] = 0xAB;   //Index 0
	adv_manuf_data_data[1] = 0xCD;   //Index 1
	adv_manuf_data_data[2] = packetData.temp[0];   //UUID
	adv_manuf_data_data[3] = packetData.temp[1];   //UUID
	adv_manuf_data_data[4] = packetData.battery[0];
	adv_manuf_data_data[5] = packetData.battery[1];
	adv_manuf_data_data[6] = crc_8(&adv_manuf_data_data[2], 4);
	memcpy(&adv_manuf_data_data[7], &timerCount, sizeof(uint32_t));
	/*
	adv_manuf_data_data[7] = 0x08;
	adv_manuf_data_data[8] = 0x09;
	adv_manuf_data_data[9] =  0x11;
	adv_manuf_data_data[10] = 0x11;         //UUID
	*/
	adv_manuf_data_data[11] = 0x12;         //UUID
	adv_manuf_data_data[12] = 0x13;         //UUID
	adv_manuf_data_data[13] = 0x14;         //UUID
	adv_manuf_data_data[14] = 0x15;         //UUID
	adv_manuf_data_data[15] = 0x16;         //UUID
	adv_manuf_data_data[16] = 0x17;         //UUID
	adv_manuf_data_data[17] = 0x18;         //UUID
	adv_manuf_data_data[18] = 0x19;         //Major
	adv_manuf_data_data[19] = 0x20;         //Major
	adv_manuf_data_data[20] = 0x21;         //Minor
	adv_manuf_data_data[21] = 0x22;         //Minor
	adv_manuf_data_data[22] = 0x23;         //TxPwer
	adv_manuf_data_data[23] = 0xAA;         //tail

	manuf_specific_data.data.p_data = adv_manuf_data_data;
	manuf_specific_data.data.size   = APP_BEACON_INFO_LENGTH;

    // Build and set advertising data.
	memset(&advdata, 0, sizeof(advdata));

	advdata.name_type             = BLE_ADVDATA_NO_NAME;
	advdata.flags                 = flags;
	advdata.p_manuf_specific_data = &manuf_specific_data;


	err_code = ble_advdata_set(&advdata, NULL);
	APP_ERROR_CHECK(err_code);

    // Initialize advertising parameters (used when starting advertising).
	memset(&advParams, 0, sizeof(advParams));

	advParams.type        = BLE_GAP_ADV_TYPE_ADV_NONCONN_IND;
	advParams.p_peer_addr = NULL;                             // Undirected advertisement.
	advParams.fp          = BLE_GAP_ADV_FP_ANY;
	advParams.interval    = NON_CONNECTABLE_ADV_INTERVAL;
	advParams.timeout     = APP_CFG_NON_CONN_ADV_TIMEOUT;
}



static void advertising_start(void){
	uint32_t err_code;
	err_code = sd_ble_gap_adv_start(&advParams);
	APP_ERROR_CHECK(err_code);

	err_code = bsp_indication_set(BSP_INDICATE_ADVERTISING);
	APP_ERROR_CHECK(err_code);
}


static void ble_stack_init(void){
	uint32_t err_code;

    // Initialize the SoftDevice handler module.
    SOFTDEVICE_HANDLER_INIT(NRF_CLOCK_LFCLKSRC_XTAL_20_PPM, NULL); //ppm20

    ble_enable_params_t ble_enable_params;
    err_code = softdevice_enable_get_default_config(CENTRAL_LINK_COUNT,   //softdevice enable
    	PERIPHERAL_LINK_COUNT,
    	&ble_enable_params);
    APP_ERROR_CHECK(err_code);

    //Check the ram settings against the used number of links
    CHECK_RAM_START_ADDR(CENTRAL_LINK_COUNT,PERIPHERAL_LINK_COUNT);

	err_code = softdevice_enable(&ble_enable_params); // softdevice enable
	APP_ERROR_CHECK(err_code);

}

void saadc_callback(nrf_drv_saadc_evt_t const * p_event){
 	if (p_event->type == NRF_DRV_SAADC_EVT_DONE) {
 		ret_code_t err_code;
 		err_code = nrf_drv_saadc_buffer_convert(p_event->data.done.p_buffer, SAMPLES_IN_BUFFER);
 		APP_ERROR_CHECK(err_code);

		memcpy(&packetData.temp, &p_event->data.done.p_buffer[0], sizeof(nrf_saadc_value_t));
		memcpy(&packetData.battery, &p_event->data.done.p_buffer[1], sizeof(nrf_saadc_value_t));
		 
		sd_ble_gap_adv_stop();
 		advertising_init();
 		advertising_start();
 	}
 }

static void power_manage(void){
	uint32_t err_code = sd_app_evt_wait();
	APP_ERROR_CHECK(err_code);
}

 // Button event handler.
void gpiote_event_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action){
	switch (pin){
		case BUTTON_1_PIN:{
			nrf_delay_ms(1000);
			if(powerState != SLEEP){
				powerState = SLEEP;
				nrf_drv_gpiote_out_clear(LED_1_PIN);
				nrf_drv_ppi_channel_disable(m_ppi_channel1);
				sd_ble_gap_adv_stop();
				power_manage();
			} else {
				nrf_drv_gpiote_out_toggle(LED_1_PIN);
				powerState = RUNNING;
				nrf_drv_ppi_channel_enable(m_ppi_channel1);
			}
			break;
		}
		case CHARGE_OK_PIN:{
			if(chargeState == CHARGE_NOW)
				chargeState = CHARGE_DONE;
			break;
		}
		case USB_CHECK_PIN :{
			switch(chargeState){
				case CHARGE_NONE :
					chargeState = CHARGE_NOW;
					break;
				case CHARGE_NOW :
					chargeState = CHARGE_NONE;
					break;
				case CHARGE_DONE :
					chargeState = CHARGE_NONE;
					break;
				default :
					break;
				}
			break;
		}
		default: break;
	}
}

 // Function for configuring GPIO.
static void gpio_config(){
	ret_code_t err_code;

	// Initialze driver.
	err_code = nrf_drv_gpiote_init();
	APP_ERROR_CHECK(err_code);

	// Configure 3 output pins for LED's.
	nrf_drv_gpiote_out_config_t out_config = GPIOTE_CONFIG_OUT_SIMPLE(false);
	err_code = nrf_drv_gpiote_out_init(LED_1_PIN, &out_config);
	APP_ERROR_CHECK(err_code);

	// Set output pins (this will turn off the LED's).
	nrf_drv_gpiote_out_set(LED_1_PIN);


	// Make a configuration for input pints. This is suitable for both pins in this example.
	nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_HITOLO(true);
	in_config.pull = NRF_GPIO_PIN_PULLUP;

	// Configure input pins for buttons, with separate event handlers for each button.
	err_code = nrf_drv_gpiote_in_init(BUTTON_1_PIN, &in_config, gpiote_event_handler);
	APP_ERROR_CHECK(err_code);

	//low to high when charge done
	nrf_drv_gpiote_in_config_t check_ok_config = GPIOTE_CONFIG_IN_SENSE_LOTOHI(true);
	check_ok_config.pull = NRF_GPIO_PIN_PULLUP;

	err_code = nrf_drv_gpiote_in_init(CHARGE_OK_PIN, &check_ok_config, gpiote_event_handler);
	APP_ERROR_CHECK(err_code);

	//connect usb
	nrf_drv_gpiote_in_config_t usb_check_config = GPIOTE_CONFIG_IN_SENSE_TOGGLE(true);
	usb_check_config.pull = NRF_GPIO_PIN_PULLUP;

	err_code = nrf_drv_gpiote_in_init(USB_CHECK_PIN, &usb_check_config, gpiote_event_handler);
	APP_ERROR_CHECK(err_code);

	// Enable input pins for buttons.
	nrf_drv_gpiote_in_event_enable(BUTTON_1_PIN, true);
	nrf_drv_gpiote_in_event_enable(USB_CHECK_PIN, true);
	nrf_drv_gpiote_in_event_enable(CHARGE_OK_PIN, true);
}

int main(){
	uint16_t count = 0;
	timerCount = 0;	
	gpio_config();
	powerState = RUNNING;
	if(nrf_gpio_pin_read(USB_CHECK_PIN))
		chargeState = CHARGE_NOW;
	else
		chargeState = CHARGE_NONE;

	ble_stack_init();

	advertising_init();

	saadc_sampling_event_init();
	saadc_init();
	saadc_samxpling_event_enable();

	advertising_start();

	while(1){
		__WFI();
		count++;
		timerCount++;
		if(count == 1000)
			count = 0;
		switch(chargeState){
			case CHARGE_NONE :{
				if(powerState == RUNNING){
					nrf_delay_ms(1000);
					nrf_drv_gpiote_out_toggle(LED_1_PIN);
				}else{
					nrf_drv_gpiote_out_clear(LED_1_PIN);
				}
				break;
			}
			case CHARGE_NOW :
				nrf_drv_gpiote_out_set(LED_1_PIN);
				break;
			case CHARGE_DONE :
				nrf_delay_ms(100);
				if(count == 2 || count == 4)
					nrf_drv_gpiote_out_toggle(LED_1_PIN);
				else
					nrf_drv_gpiote_out_clear(LED_1_PIN);
				break;
			default : break;
		}
	}
}
