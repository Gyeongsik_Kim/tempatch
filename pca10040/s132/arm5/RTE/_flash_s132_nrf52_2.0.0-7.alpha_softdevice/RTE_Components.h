
/*
 * Auto generated Run-Time-Environment Component Configuration File
 *      *** Do not modify ! ***
 *
 * Project: 'ble_app_beacon_s132_pca10040' 
 * Target:  'flash_s132_nrf52_2.0.0-7.alpha_softdevice' 
 */

#ifndef RTE_COMPONENTS_H
#define RTE_COMPONENTS_H


/*
 * Define the Device Header File: 
 */
#define CMSIS_device_header "nrf.h"

#define GPIOTE_CONFIG_NUM_OF_LOW_POWER_EVENTS
#define GPIOTE_ENABLED
  #define GPIOTE_CONFIG_NUM_OF_LOW_POWER_EVENTS
#define S132
#define SAADC_ENABLED
#define SOFTDEVICE_PRESENT
#define SWI_DISABLE0

#endif /* RTE_COMPONENTS_H */
